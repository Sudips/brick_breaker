#include<sdl/sdl.h>
#include<sdl_opengl.h>
#include<sdl/sdl_image.h>
#include<gl/gl.h>
#include<gl/glu.h>
#include "tex.h"
#define NO_OF_BRICKS 12*5


GLuint texPad,texBall,texBrick,texBack;

int CollisionCheck(float Ax,float Ay,float Aw,float Ah,float Bx,float By,float Bw,float Bh)
{
    if(Ay+Ah<=By) return false;
    else if(Ay>By+Bh) return false;
    else if(Ax+Aw<Bx) return false;
    else if(Ax>Bx+Bw) return false;
    return true;
}

struct Brick{
              float brickX;
              float brickY;
              float brickW;
              float brickH;
              bool alive;
            };
void Display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glPushMatrix();
}

int main(int argc, char* argv[])
{
    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_WM_SetCaption("Brick-Break",NULL);  //SET WINDOW CAPTION

    SDL_SetVideoMode(665,480,32,SDL_OPENGL|SDL_HWPALETTE);  //SET WINDOW SIZE
    SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute( SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

    glClearColor(0xff,0xff,0xff,0xff); //SET INITIAL BACKGROUND COLOR

    glShadeModel(GL_SMOOTH);

    glMatrixMode(GL_PROJECTION);  //For 2D rendering
    glLoadIdentity();

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    gluOrtho2D(0,665,480,0);
    glViewport(0,0,665,480);
    glEnable(GL_TEXTURE_2D);
    texPad=loadTexture("pad.png");
    texBall=loadTexture("ball.png");
    texBack=loadTexture("back.png");
    texBrick=loadTexture("brick.png");


    //Main game loop

    //handles the main loop
     bool isRunning=true;

    //for handling with event
    SDL_Event event;

    //Pad variables
    float x=320,y=455,width=80,hight=30;
    int left=false,right=false;

    //Ball variables
    float ballx=(x+width/3),bally=(y-hight),ballWH=30; //Hight-Width is same
    float velx=0.4,vely=0.4;
    bool start=false;//Game will not strat untill player press UP arrow or w
    int count=0; //All bricks over then game over
    //int chance=0;

    struct Brick bricks[NO_OF_BRICKS];

    for(int i=0,Bx=5,By=5;i<NO_OF_BRICKS;i++,Bx+=55)
    {
        if(Bx>650)
        {
            Bx=5;
            By+=30;
        }
        bricks[i].brickX=Bx;
        bricks[i].brickY=By;
        bricks[i].brickW=50;
        bricks[i].brickH=20;
        bricks[i].alive=true;
    }

   while(isRunning)
    {
        //input or enevts
        while(SDL_PollEvent(&event))
        {
            if(event.type==SDL_QUIT)
            {
                isRunning = false;
            }
            if(event.type==SDL_KEYDOWN && event.key.keysym.sym==SDLK_ESCAPE)
            {
                isRunning = false;
            }

            //checks whether key pressed
            if(event.type==SDL_KEYDOWN)
            {
                if(event.key.keysym.sym==SDLK_LEFT||event.key.keysym.sym==SDLK_a)
                {
                    left=true;
                }
                if(event.key.keysym.sym==SDLK_RIGHT||event.key.keysym.sym==SDLK_d)
                {
                    right=true;
                }
                if(event.key.keysym.sym==SDLK_UP||event.key.keysym.sym==SDLK_w)
                {
                    start=true;
                }
            }

            //checks whether key released
            else if(event.type==SDL_KEYUP)
            {
                if(event.key.keysym.sym==SDLK_LEFT||event.key.keysym.sym==SDLK_a)
                {
                    left=false;
                }
                if(event.key.keysym.sym==SDLK_RIGHT||event.key.keysym.sym==SDLK_d)
                {
                    right=false;
                }
            }
        }
        //LOGIC

        //Pad move locic
        if(left==true&&start==true)
        {
            x-=0.7;
        }
        if(right==true&&start==true)
        {
            x+=0.7;
        }
        if(x<0)
        {
            x=0;
        }
        if(x+width>665)
        {
            x=665-width;
        }

       //Ball move logic
       if(start==true)
       {
           ballx -= velx;
           bally -= vely;
       }
       if(ballx<0)
       {
           velx = -velx;
       }
       else if(ballx+ballWH>665)
       {
           velx = -velx;
       }
       if(bally<0)
       {
           vely = -vely;
       }
       else if(bally+ballWH>=480)
       {
           //isRunning=false;
           vely = -vely;
       }
       //collision checking slider-ball
       if(CollisionCheck(ballx,bally,ballWH,ballWH,x,y,width,hight)==true&&start==true)
       {
           vely = -vely;
           float temp = ballx+ballWH/2;
           temp = temp - x;
           temp = temp - 40;
           temp = temp / 100;
           velx = temp;
       }
       //collision checking ball-brick

       if(start==true)
       {
          for(int i=0;i<NO_OF_BRICKS;i++)
          {

             if(CollisionCheck(bricks[i].brickX,bricks[i].brickY,bricks[i].brickW,bricks[i].brickH,ballx,bally,ballWH,ballWH)==true)
             {
                 bricks[i].alive=false;
                 vely=-vely;
                 count++;
             }
          }
       }

        if(count==NO_OF_BRICKS)
             isRunning=false;
        //start rendering

        Display();
        glBindTexture(GL_TEXTURE_2D,texBack);
        BindImg(0,0,665,480);

        glBindTexture(GL_TEXTURE_2D,texPad);
        BindImg(x,y,width,hight);

        glBindTexture(GL_TEXTURE_2D,texBall);
        BindImg(ballx,bally,ballWH,ballWH);

        glBindTexture(GL_TEXTURE_2D,texBrick);
        for(int i=0;i<NO_OF_BRICKS;i++)
        {
            if(bricks[i].alive==true)

               BindImg(bricks[i].brickX,bricks[i].brickY,bricks[i].brickW,bricks[i].brickH);

            else
            {
                bricks[i].brickX=-1.0;
                bricks[i].brickY=-1.0;
                bricks[i].brickW=-1.0;
                bricks[i].brickH=-1.0;
            }
        }
        glPopMatrix();

        SDL_GL_SwapBuffers();
        //SDL_Delay(2);
        //SDL_Flip(screen);
    }
    SDL_Quit();
    return 0;
}
