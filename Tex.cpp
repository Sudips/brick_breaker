#include<sdl/sdl.h>
#include<sdl/sdl_opengl.h>
#include<sdl/sdl_image.h>

GLuint loadTexture(const char* filename)
{
  SDL_Surface *image = IMG_Load(filename);

  SDL_DisplayFormatAlpha(image);


  GLuint object;

  glGenTextures(1, &object);

  glBindTexture(GL_TEXTURE_2D, object);

  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->pixels);
  SDL_SetColorKey(image,SDL_SRCCOLORKEY,SDL_MapRGB(image->format,0x00,0xff,0xff));

  //Free surface
  SDL_FreeSurface(image);

  return object;
}

void BindImg(float x,float y,float w,float h)
{
    glBegin(GL_QUADS);
    glColor3f(1.0,1.0,1.0);
    glTexCoord2f(0.0,0.0);
    glVertex2f(x,y);
    glTexCoord2f(0.0,1.0);
    glVertex2f(x,y+h);
    glTexCoord2f(1.0,1.0);
    glVertex2f(x+w,y+h);
    glTexCoord2f(1.0,0.0);
    glVertex2f(x+w,y);
    glEnd();
}
